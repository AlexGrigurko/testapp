package company;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class MainTest {



    @Test
    public void reverse_string() {
        String given = "12345678";

        String result = Reverse.reverseString(given);

        assertEquals("87654321", result);
    }

    @Test
    public void sort_words() {
        String given = "Hi my name is Jil nice to meet you Alex";

        Map<Integer, List<String>> result = Sorting.SortWords(given);

        assertEquals(3, result.size());
        assertNotNull(result.get(2));
        //assertEquals(result.get(2).size(), );
    }

    @Test
    public void valid_phone() {
        assertTrue( Validation.Valid("+380955099940"));
        assertFalse( Validation.Valid("+355099940"));
        assertTrue( Validation.Valid("380955099940"));
        assertFalse( Validation.Valid("0955099940"));

    }

//    @Test
//    public void num_sum(){
//
//        List<Integer> given = new ArrayList<>();
//        given.add(2);
//        given.add(3);
//        given.add(5);
//        given.add(4);
//        given.add(6);
//
//        int result = NumOperations.sumNum(given);
//
//        assertEquals(20, result);
//    }

    @Test
    public void col_words(){

        String given = "Alex Alex Alex Sasha Sasha Dima";

        Map<String, Integer> result = Sorting.colWords(given);

        assertNotNull(result.get("Alex"));
        assertEquals(3, result.size());

    }
    @Test
    public void string_reverse(){

        String given = "1234567890";

        String result = Reverse.reverseString(given);

        assertEquals("0987654321", result);
    }



}