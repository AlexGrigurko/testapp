package company;

public class Reverse {
    public static String reverseString(String in){
        StringBuffer buffer = new StringBuffer(in);
        buffer.reverse();
        return buffer.toString();
    }
}
