package company;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import static company.Validation.Valid;

public class SimpleInput {



    static void simplInput() {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter name: ");
        String name = sc.next();

        System.out.print("Enter age: ");
        int age = sc.nextInt();

        String pnum;
        do {
            System.out.print("Enter phone number: ");
            pnum = sc.next();
            if (Valid(pnum) == false) {
                System.out.print("Please, write correct phnone number!" + "\n");
                continue;
            }
        }
        while (!Valid(pnum));

    }
}
