package company;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class Main {
    public static void main(String[] args) {











        Scanner sc = new Scanner(System.in);
        String choice;
        do {
            System.out.println("\n" + "Hey, we can do these things: " +
                    "\n" + "1. Simple user input " +
                    "\n" + "2. Enter numbers" +
                    "\n" + "3. Unique numbers" +
                    "\n" + "4. Sum of numbers" +
                    "\n" + "5. To trim all space" +
                    "\n" + "0. Exit");

            choice = sc.next();
            switch (choice) {
                /*case "1":
                    simplInput();
                    break;
                case "2":
                    List<Integer> numbers = enterNum();
                    System.out.println(numbers);
                    break;
                case "3":
                    uniqueNum();
                    break;
                case "4":
                    int sum = sumNum();
                    System.out.print(sum);
                    break;
                case "0":
                    break;
                case "5":
                    trim();
                    break;
                case "6":
                    colSize();
                    break;*/
                case "7":
                    ReverseWordsExtractor reverseWordsExtractor = new ReverseWordsExtractor();
                    Runnable command = new ReverseWordsCommand(reverseWordsExtractor);
                    command.run();
                    break;
                case "8":
                    Sorting sorting = new Sorting();
                    String text = sc.nextLine();
                    sorting.SortWords(text);
                    break;
            }
        } while (!choice.equals("0"));
    }
    static class ReverseWordsCommand implements Runnable {
        private ReverseWordsExtractor exctractor;

        ReverseWordsCommand(ReverseWordsExtractor exctractor) {
            this.exctractor = exctractor;
        }

        @Override
        public void run() {
            Scanner scWrd = new Scanner(System.in);
            String inptWrd = scWrd.nextLine();

            Queue<String> strings = exctractor.extract(inptWrd, " ");

            for (String string : strings) {
                System.out.println(string);
            }
        }
    }

    static class ReverseWordsExtractor {
        public Queue<String> extract(String sentence, String delimiter) {
            String[] split = sentence.split(delimiter);

            Queue<String> strings = new ArrayDeque<>(split.length);
            for (int i = split.length - 1; i >= 0; i -= 1) {
                strings.add(split[i]);
            }
            return strings;
        }
    }

    static void reverseWords() {

        Scanner scWrd = new Scanner(System.in);
        String inptWrd = scWrd.nextLine();


        String[] split = inptWrd.split(" ");

        for (int i = split.length - 1; i == 0; i--) {
            System.out.println(split[i]);
        }
    }

    }



