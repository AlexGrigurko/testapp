package company;

import java.util.Queue;

public class Test {

    public static void main(String[] args) {
        Main.ReverseWordsExtractor exctractor = new Main.ReverseWordsExtractor();

        Queue<String> extract = exctractor.extract("a sentence with words", " ");

        for (String s : extract) {
            System.out.println(s);
        }
    }
}
