package company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {
    public static boolean Valid(String pnum) {
        Pattern pattern = Pattern.compile("^((\\+?380)([0-9]{9}))$");
        Matcher matcher = pattern.matcher(pnum);
        return matcher.matches();
    }
}
