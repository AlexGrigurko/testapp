//package company;
//
//import java.util.ArrayList;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class NumOperations {
//    static int sumNum(List<Integer> numbers) {
//
//        int sum = 0;
//        for (Integer number : numbers) {
//            sum = sum + number;
//        }
//        return sum;
//    }
//    static void uniqueNum() {
//        String inputNum;
//        List<Integer> numbers = new LinkedList<>(); //Можно было использовать LinckedHashMap
//        System.out.print("Please, enter the number: " + "\n" + "Or, press '.', to see numbers: ");
//
//        do {
//            inputNum = sc.next();
//            try {
//                int parseInt = Integer.parseInt(inputNum);
//                numbers.add(parseInt);
//
//            } catch (NumberFormatException e) {
//                if (!inputNum.equals(".")) {
//                    System.out.println("Insert only numbers!");
//                } else {
//                    e.printStackTrace();
//                }
//            }
//        } while (!inputNum.equals("."));
//        System.out.print(numbers.stream().distinct().collect(Collectors.toList()) ); //что делает collect...
//    }
//    static List<Integer> enterNum(String inputNum) {
//
//        List<Integer> numbers = new ArrayList<>();
//
//        do {
//            inputNum = scanner.next();
//            try {
//                int parseInt = Integer.parseInt(inputNum);
//                numbers.add(parseInt);
//            } catch (NumberFormatException e) {
//                if (!inputNum.equals(".")) {
//                    System.out.println("Insert only numbers!");
//                }
//            }
//        } while (!inputNum.equals("."));
//        return numbers;
//    }
//}
