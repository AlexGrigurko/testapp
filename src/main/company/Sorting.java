package company;

import java.util.*;

public class Sorting implements XCommand {

    public static Map<Integer, List<String>> SortWords(String in) {
        Scanner scanner = new Scanner(System.in);
        Map<Integer, List<String>> counter = new HashMap<>();
        String[] wordList = in.split(" ");


        for (int i = 0; i < wordList.length; i++) {
            String s = wordList[i];
            Integer key = s.length();

            if (counter.containsKey(key)) {
                counter.get(key).add(s);
            } else {
                List<String> al = new ArrayList<>();
                al.add(s);
                counter.put(key, al);
            }
        }
        return counter;
    }
    public static Map<String, Integer> colWords(String wrd) {

        Map<String, Integer> words = new HashMap<>();
//        do {
        String[] split = wrd.split(" ");
        for (String s : split) {
            Integer counter = words.get(s);
            if (counter == null) {
                words.put(s, 1);
            } else {
                words.put(s, counter + 1);
            }
        }
//        } while (wrd.equals("."));
//        for (Map.Entry<String, Integer> entry : words.entrySet()) {
//            System.out.println(entry.getKey() + "=" + entry.getValue());
//        }
        return words;
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public void run() {

    }
    /*static void colSize() {

        Scanner scSim = new Scanner(System.in);
        String inputSymb = scSim.nextLine();
        System.out.println(inputSymb.length());
    }*/

}
